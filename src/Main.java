import java.util.Random;
import java.util.Scanner;

import static java.lang.System.*;

public class Main {
    public static void main(String[] args) {
        int gameField[][] = new int[20][20]; //объявляю массив
        View field = new View();
        Random random = new Random();
        Scanner scanner = new Scanner(in);
        Control step = new Control();
        int one = random.nextInt(gameField.length -1); //определяю строку
        int two = random.nextInt(gameField.length -1); //определяю столбец
        field.box(gameField, one, two, gameField.length-1, gameField[0].length-1); //передаю в метод box, отрисовывается массив
        String action = scanner.next(); //считываю направление движения
        while (!action.equals("exit")){
            try { step.moveItMoveIt(gameField, action);}
            catch (ArrayIndexOutOfBoundsException ignore){break;}
            action = scanner.next();
        }
        System.out.println("Досвидули!");
    }
}
