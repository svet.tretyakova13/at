import java.util.Random;

public class Control {
    int r1, r2;
    Random random = new Random();
    EndGame theEnd = new EndGame();
     /*
    act - команда пользователя
    w,x - размер массива
    r,l - позиция игрока
    r1 - строка
    r2 - столбец
     */

    public void moveItMoveIt(int arr[][], String act) {
        int gamer = 7;
        int monster = 3;
        int strG, stG, strM, stM;
        View viewer = new View();
        String pozG = viewer.search(arr, gamer); //определяю позицию игрока
        String rezG[] = pozG.split(",");
        strG = Integer.valueOf(rezG[0]);
        stG = Integer.valueOf(rezG[1]);
        String pozM = viewer.search(arr, monster); //определяю позицию монстра
        String rezM[] = pozM.split(",");
        strM = Integer.valueOf(rezM[0]);
        stM = Integer.valueOf(rezM[1]);
        switch (act) {
            case "w":
                r1 = strG - 1;
                r2 = stG;
                break;
            case "s":
                r1 = strG + 1;
                r2 = stG;
                break;
            case "a":
                r1 = strG;
                r2 = stG - 1;
                break;
            case "d":
                r1 = strG;
                r2 = stG + 1;
                break;
        }
        if (r1 < 0 | r1 > arr.length | r2 < 0 | r2 > arr[0].length) {
            theEnd.hitWall();
        }

        if (r1==strM){
            if (r2==stM){
                theEnd.kill();
                r1=-1;
                r2=-1;
            }
        }
        strM=strG;
        stM=stG;
        viewer.box(arr, r1, r2, strM, stM);
    }
}